# evotest/main.py

from pprint import pprint


def update(data, service, count):

    sum_instances = {}
    # get sum of instances for every host
    for host, instance in data.items():
        sum_instances.update({host: sum(instance.values())})

    values = list(sum_instances.values())
    keys = list(sum_instances.keys())
    # get host with min and max amounts of values
    host_min = min(sum_instances, key=sum_instances.get)
    host_max = max(sum_instances, key=sum_instances.get)

    # check if number of instances are the same
    if values[0] == values[1]:
        host_min = keys[0]
        host_max = keys[1]

    # get delta to determine server for updating
    delta = sum_instances[host_min] + count - sum_instances[host_max]

    # update server with min amount of instances
    if delta <= 0:
        amount_min_server = data[host_min].get(service)
        if amount_min_server:
            count += amount_min_server
        data[host_min].update({service: count})
    else:
        # get total amount of instances
        total_instances = sum(sum_instances.values()) + count
        # we need to allocate evenly
        middle = total_instances//2
        # split count to two parts
        first_part = count - (middle - sum_instances[host_max])
        second_part = count - first_part
        # check if services exists ...
        amount_min_server = data[host_min].get(service)
        amount_max_server = data[host_max].get(service)
        # ... and sum with new part
        first_part += amount_min_server if amount_min_server else 0
        second_part += amount_max_server if amount_max_server else 0
        # update config
        data[host_min].update({service: first_part})
        data[host_max].update({service: second_part})


def main():
    example_data = {
        'ginger': {
            'django': 2,
            'flask': 3,
        },
        'cucumber': {
            'flask': 1,
        },
    }

    print("Configuration before:")
    pprint(example_data)

    update(example_data, 'pylons', 7)

    print("Configuration after:")
    pprint(example_data)

if __name__ == '__main__':
    main()
